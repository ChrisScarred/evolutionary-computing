"""Visualisations of results.
"""

import os
from textwrap import wrap
from typing import Dict, List, Optional, Union
from seed import set_numpy_seed

import logomaker
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

plt.tight_layout()

def _get_title(title: str, max_line_len: int = 60) -> str:
    return "\n".join(wrap(title, max_line_len))


def visualise_selection_probs(
    probs: List[float],
    labels: Optional[List[str]] = None,
    title: Optional[str] = None,
    visuals_dir: Optional[str] = None,
    file_name: Optional[str] = None,
) -> None:
    """Visualise selection probabilities.

    Args:
        probs (List[float]): Probabilities of selecting each individual.
        labels (Optional[List[str]], optional): Labels of individuals. If None, no labels are shown.
        Defaults to None.
        title (Optional[str], optional): Title of the figure. If None, no title is shown. Defaults to None.
        visuals_dir (Optional[str], optional): The directory to store the figure. If None, the figure is
        not saved. Defaults to None.
        file_name (Optional[str], optional): The fine name of the saved figure. If None and visual_dir
        is an instance of string,
        saves the figure under the name "selection_probs". Defaults to None.
    """
    fig, ax = plt.subplots()
    ax.pie(probs, labels=None, autopct="%1.1f%%", textprops={"fontsize": 20})
    if labels:
        ax.legend(labels=labels, loc="center left", bbox_to_anchor=(1, 0.5))
    if title:
        fig.suptitle(_get_title(title))
    plt.show()
    save_fig(fig, visuals_dir, file_name if file_name else "selection_probs")


def visualise_multi_f_selection_probs(
    probs_per_f: Dict[str, List[float]],
    population: List[int],
    visuals_dir: Optional[str] = None,
    name_prefix: Optional[str] = None,
) -> None:
    """Performs multiple calls to visualise_selection_probs.

    Args:
        probs_per_f (Dict[str, List[float]]): The names of the functions (keys) and their selection
        probabilities (values).
        population (List[int]): The population.
        visuals_dir (Optional[str], optional): The directory to store the figure. If None, the figure is
        not saved. Defaults to None.
        name_prefix (Optional[str], optional): The prefix to give each figure file name. If None, no prefix
        is used. Defaults to None.
    """
    labels = [f"x = {n}" for n in population]
    for f_name, probs in probs_per_f.items():
        visualise_selection_probs(
            probs,
            labels=labels,
            title=f"Selection Probabilities under Fitness Function {f_name}",
            visuals_dir=visuals_dir,
            file_name=f"{name_prefix}{f_name}" if name_prefix else f_name,
        )


def save_fig(
    fig: plt.Figure,
    visuals_dir: Optional[str],
    file_name: Optional[str],
):
    """Attempt to save a figure.

    Args:
        fig (plt.Figure): The figure to save.
        visuals_dir (Optional[str]): The path to the directory to store the figure.
        If None, does not save the figure.
        file_name (Optional[str]): The file name under which to save the figure.
        If None and visual_dir is an instance of string, saves the figure under the name fig.png.
    """
    if visuals_dir:
        name = ""
        if file_name:
            name = os.path.join(visuals_dir, f"{file_name}.png")
        else:
            name = os.path.join(visuals_dir, "fig.png")
        fig.savefig(name, bbox_inches="tight")


def plot_fitnesses(
    fitnesses: Union[List[Union[int, float]], Dict[str, List[Union[int, float]]]],
    title: Optional[str],
    visuals_dir: Optional[str],
    file_name: Optional[str],
    max_fitness: Optional[Union[int, float]] = None,
    range_key: Optional[str] = None,
) -> None:
    """Plot the fitness per generation, optionally for multiple conditions.

    Args:
        fitnesses (Union[List[Union[int, float]], Dict[str, List[Union[int, float]]]]):
        A list of fitnesses per generation or the name of the condition (key) and the list
        of fitnesses per generation under that condition (value).
        title (Optional[str]): The title of the figure. If None, no title is shown.
        visuals_dir (Optional[str]): The path to the directory to store the figure.
        If None, does not save the figure.
        file_name (Optional[str]): The file name under which to save the figure.
        If None and visual_dir is an instance of string, saves the figure under the name fitness.png.
        max_fitness (Optional[Union[int, float]]): The maximal fitness achievable. If a value is supplied,
        the threshold is visualised. Defaults to None.
        range_key (Optional[str]): If supplied, draw a range of the values of this model as a rectagle in
        the background. Defaults to None.
    """
    fig, ax = plt.subplots()
    if isinstance(fitnesses, dict):
        for name, fitness in fitnesses.items():
            ax.plot(np.array(fitness), label=name)
        plt.legend()
    else:
        ax.plot(fitnesses)
    ax.set_xlabel("Generations")
    ax.set_ylabel("Best Fitness")
    if title:
        fig.suptitle(_get_title(title))

    if range_key:
        mn = min(fitnesses.get(range_key, [0]))
        mx = max(fitnesses.get(range_key, [0]))
        if mn != mx:
            plt.axhspan(mn, mx, facecolor="tab:orange", alpha=0.2)

    if max_fitness:
        if isinstance(fitnesses, dict):
            fitnesses = [fitness for s in fitnesses.values() for fitness in s]
        if max_fitness in fitnesses:
            index_max = fitnesses.index(max_fitness)
            ax.axvline(x=index_max, color="gray", linestyle="--")

    plt.show()
    save_fig(fig, visuals_dir, file_name if file_name else "fitness")


def beeswarm_plot(
    data: Dict[Union[float, str], List[float]],
    title: Optional[str] = None,
    visuals_dir: Optional[str] = None,
    file_name: Optional[str] = None,
    xlabel: Optional[str] = None,
    ylabel: Optional[str] = None,
):
    """Plot a beeswarm plot.

    Args:
        data (Dict[Union[float, str], List[float]]): The data as a condition name (key) and values
        associated with it (value).
        title (Optional[str], optional): The title of the figure. If None, no title is shown.
        Defaults to None.
        visuals_dir (Optional[str], optional): The path to the directory to store the figure.
        If None, does not save the figure. Defaults to None.
        file_name (Optional[str], optional): The file name under which to save the figure.
        If None and visual_dir is an instance of string, saves the figure under the name fig.png.
        Defaults to None.
        xlabel (Optional[str], optional): The label of the x axis. If None, no label is shown.
        Defaults to None.
        ylabel (Optional[str], optional): The label of the y axis. If None, no label is shown.
        Defaults to None.
    """
    set_numpy_seed()
    fig, ax = plt.subplots()
    sns.stripplot(data, ax=ax)
    if title:
        ax.set_title(_get_title(title))
    if xlabel:
        ax.set_xlabel(xlabel)
    if ylabel:
        ax.set_ylabel(ylabel)
    if isinstance(list(data.keys())[0], float):
        ax.set_xticklabels([round(mu, 4) for mu in data.keys()])
    plt.show()
    save_fig(fig, visuals_dir=visuals_dir, file_name=file_name)


def multi_condition_beeswarm(
    data: Dict[str, Dict[str, List[float]]],
    title: Optional[str] = None,
    visuals_dir: Optional[str] = None,
    file_name_prefix: Optional[str] = None,
) -> None:
    """Plot a beeswarm plot for each upper-level condition.

    Args:
        data (Dict[str, Dict[str, List[float]]]): Data as the name of the upper level condition (key),
        the name of the lower level condition (key) and the values associated with the conditions (value).
        title (Optional[str], optional): The title of the plots. If None, no title is shown.
        Defaults to None.
        visuals_dir (Optional[str], optional): The path to the directory to store the figure.
        If None, does not save the figure. Defaults to None.
        file_name_prefix (Optional[str], optional): The fine name of the saved figure.
        If None and visual_dir is an instance of string, saves the figure under the name "fig_0<i+1>"
        with <i+1> being the index. Defaults to None.
    """
    for i, (name, results) in enumerate(data.items()):
        beeswarm_plot(
            results,
            title=title.replace("<name>", name) if title else None,
            visuals_dir=visuals_dir,
            file_name=(
                f"{file_name_prefix}fig_0{i+1}" if file_name_prefix else f"fig_0{i+1}"
            ),
            xlabel="Algorithm",
            ylabel="Maximum Fitness",
        )


def _pad_results(results: List[List[float]]) -> np.ndarray:
    """Pad results in a list of lists such that each sublist has an equal length."""
    lens = [len(r) for r in results]
    mx = max(lens)
    if min(lens) < mx:
        return np.array([np.pad(np.array(x), (0, mx - len(x))) for x in results])
    return np.array(results)


def diversity_plot_distances(
    data: Dict[float, np.ndarray],
    max_gens: int,
    title: Optional[str] = None,
    visuals_dir: Optional[str] = None,
    file_name: Optional[str] = None,
):
    """Plot the population diversity under different conditions.

    Args:
        data (Dict[float, np.ndarray]): Results as the condition name (key) and the average distances
        per recorded generation (value).
        max_gens (int): The maximum allowed number of generations for the experiment to which
        these results correspond.
        title (Optional[str], optional): The title of the plot. If None, no title is shown. Defaults to None.
        visuals_dir (Optional[str], optional): The path to the directory to store the figure.
        If None, does not save the figure. Defaults to None.
        file_name (Optional[str], optional): The file name under which to save the figure.
        If None and visual_dir is an instance of string, saves the figure under the name fig.png.
        Defaults to None.
    """
    fig, ax = plt.subplots()
    if title:
        fig.suptitle(_get_title(title))
    ax.set_xlabel("Generations")
    ax.set_ylabel("Mean Pairwise Hamming Distance")
    n = len(ax.get_xticks())
    step = max_gens / n
    ax.set_xticklabels([round(step * x) for x in range(n)])
    for rate, results in data.items():
        results = _pad_results(results)
        ax.plot(np.mean(np.array(results), axis=0), label=f"$\mu =$ {rate}")
    ax.legend()
    plt.show()
    save_fig(fig, visuals_dir=visuals_dir, file_name=file_name)


def plot_sequence_logos(
    data: Dict,
    k: int,
    visuals_dir: Optional[str] = None,
    file_name_prefix: Optional[str] = None,
):
    """Plot sequence logos of the last generation under a specific k value for each mu value.

    Args:
        data (Dict): Data to plot as the mutation probability mu (key) and the last population (value).
        k (int): The value of k to which these results correspond.
        visuals_dir (Optional[str], optional): The path to the directory to store the figure.
        If None, does not save the figure. Defaults to None.
        file_name_prefix (Optional[str], optional): The fine name of the saved figure. If None
        and visual_dir is an instance of string,
        saves the figure under the name "fig_03_k<k>_0<i+1>" with <k> being the value of k and <i+1>
        being the index. Defaults to None.
    """
    for i, (mu, seq_list) in enumerate(data.items()):
        fig, ax = plt.subplots()
        sequences = [subject for seq in seq_list for subject in seq]
        counts = logomaker.alignment_to_matrix(sequences)
        fig.suptitle(
            _get_title(
                f"Last Generation of StringFinder with $\mu =$ {mu} and $K = {k}$"
            )
        )
        logomaker.Logo(counts, ax=ax)
        plt.show()
        save_fig(
            fig,
            visuals_dir=visuals_dir,
            file_name=(
                f"{file_name_prefix}fig_03_k{k}_0{i+1}"
                if file_name_prefix
                else f"fig_03_k{k}_0{i+1}"
            ),
        )


def visualise_string_finders(
    results: Dict,
    ks: List[int],
    max_gens: int,
    visuals_dir: Optional[str] = None,
    file_name_prefix: Optional[str] = None,
):
    """_summary_

    Args:
        results (Dict): The results as the mutation rate mu (key) and a dictionary of value of k (key)
        and a dictionary of metrics type (key) and values of that metric (values). Metrics should include
        "t_finish", "distances", and "last_populations".
        ks (List[int]): The list of values of k used in the experiment whose results are being visualised.
        max_gens (int): The maximum number of generations used in the experiment whose results are
        being visualised.
        visuals_dir (Optional[str], optional): The path to the directory to store the figure.
        If None, does not save the figure. Defaults to None.
        file_name_prefix (Optional[str], optional): The fine name of the saved figure. If None and
        visual_dir is an instance of string,
        use default prefixes for each visualisation type. Defaults to None.
    """
    for k in ks:
        beeswarm_plot(
            {mu: res[k]["t_finish"] for mu, res in results.items()},
            title=f"Distribution of Number of Generations $t_{{finish}}$ to Convergence for Different Values of $\mu$ in StringFinder with $K={k}$",
            visuals_dir=visuals_dir,
            file_name=(
                f"{file_name_prefix}fig_01_k{k}" if file_name_prefix else f"fig_01_k{k}"
            ),
            xlabel="$\mu$",
            ylabel="$t_{finish}$",
        )
        diversity_plot_distances(
            {mu: res[k]["distances"] for mu, res in results.items()},
            max_gens,
            f"Average Population Diversity as Mean Hamming Distance over Individuals and Trials for Different Values of $\mu$ in StringFinder with $K={k}$",
            visuals_dir=visuals_dir,
            file_name=(
                f"{file_name_prefix}fig_02_k{k}" if file_name_prefix else f"fig_02_k{k}"
            ),
        )
        plot_sequence_logos(
            {mu: res[k]["last_populations"] for mu, res in results.items()},
            k=k,
            visuals_dir=visuals_dir,
            file_name_prefix=file_name_prefix,
        )
