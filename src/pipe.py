"""A collection of reusable actions composed of multiple processes and the processes.
"""

import fractions
import os
import pickle
from typing import Callable, Dict, List, Optional

import numpy as np
from data import read_tsp
from genetic_algorithm import GeneticAlgorithm

from visuals import (
    multi_condition_beeswarm,
    plot_fitnesses,
    visualise_multi_f_selection_probs,
    visualise_string_finders,
)


def fitness_of_population(f: Callable, n: List[float]) -> List[float]:
    """Given a fitness function and a population, calculate the fitness of the population."""
    fitness = lambda f, x: f(x)
    return [fitness(f, x) for x in n]


def fitnesses_of_population(
    fs: Dict[str, Callable], n: List[float]
) -> Dict[str, List[float]]:
    """Given multiple fitness functions, calculate the fitness of the population
    under each fitness function.
    """
    return {name: fitness_of_population(f, n) for name, f in fs.items()}


def selection_probs(fitnesses: List[float]) -> List[float]:
    """Given a list of fitnesses, calculate the selection probabilities of each subject."""
    sum_ = sum(fitnesses)
    return [n / sum_ for n in fitnesses]


def multi_f_selection_probs(
    fitnesses_per_f: Dict[str, List[float]]
) -> Dict[str, List[float]]:
    """Given fitnesses under multiple conditions, calculate the selection probabilities
    under each condition for each subject.
    """
    return {name: selection_probs(n) for name, n in fitnesses_per_f.items()}


def selection_probs_multi_functions(
    fitness_functions: Dict[str, Callable],
    population: List[float],
    visualise: bool = False,
    visuals_dir: str = "figures",
) -> Dict[str, List[float]]:
    """Calculate the selection probabilities under different fitness functions for each
    subject of the population and optionally visualise the results.

    Args:
        fitness_functions (Dict[str, Callable]): The names of the functions (key) and
        the functions themselves (value).
        population (List[float]): The population.
        visualise (bool, optional): Whether or not to visualise. Defaults to False.
        visuals_dir (str, optional): The path to the directory that stores figures.
        Defaults to "figures".

    Returns:
        Dict[str, List[float]]: The selection probabilities for each function.
    """
    fitnesses = fitnesses_of_population(fitness_functions, population)
    probs = multi_f_selection_probs(fitnesses)
    if visualise:
        visualise_multi_f_selection_probs(probs, population, visuals_dir, "ex_01_")
    return probs


def one_plus_one_ga(
    l: int,
    mu: float,
    gens: int,
    model: GeneticAlgorithm,
    visualise: bool = False,
    visuals_dir: str = "figures",
    file_name: Optional[str] = None,
    max_fitness: Optional[int] = None,
) -> List[int]:
    """Perform a trial of (1+1) GA.

    Args:
        l (int): the population size.
        mu (float): the mutation probability.
        gens (int): the number of generations.
        model (GeneticAlgorithm): the model.
        visualise (bool, optional): Whether or not to visualise the results. Defaults to False.
        visuals_dir (str, optional): The path to the directory that stores the results. Defaults to "figures".
        file_name (Optional[str], optional): The file name of figure. If visualise is True and file_name is None,
        the figure is saved under the name max_fitness_1+1_ga. Defaults to None.

    Returns:
        List[int]: The list of maximum fitnesses per generation.
    """
    ga = model(mu, l, gens)
    _, results = ga.run()
    if visualise:
        plot_fitnesses(
            results,
            "Maximum Fitness per Generation in (1+1)-GA",
            visuals_dir=visuals_dir,
            file_name=file_name if file_name else "max_fitness_1+1_ga",
            max_fitness=max_fitness,
        )
    return results


def multi_trial_one_plus_one_ga(
    l: int, mu: float, gens: int, trials: int, model: GeneticAlgorithm
) -> List[float]:
    """Perform multiple trials of (1+1) GA and return the average metric."""
    trial = lambda _: one_plus_one_ga(l, mu, gens, model)
    return list(np.mean(np.array([trial(x) for x in range(trials)]), axis=0))


def multi_model_one_plus_one(
    l: int,
    mu: float,
    gens: int,
    trials: int,
    models: Dict[str, Dict],
    visualise: bool = False,
    visuals_dir: str = "figures",
    file_name_prefix: Optional[str] = None,
    range_key: Optional[str] = None,
):
    """Perform multiple trials of multiple models of (1+1) GA."""
    results = {}
    for i, (model_name, model_dict) in enumerate(models.items()):
        model = model_dict.get("model")
        vis = model_dict.get("visualise")
        results[model_name] = {
            "one_run": one_plus_one_ga(
                l,
                mu,
                gens,
                model,
                visualise=vis if visualise else False,
                visuals_dir=visuals_dir,
                file_name=(
                    f"{file_name_prefix}fig_0{i+1}"
                    if file_name_prefix
                    else f"max_fitness_1+1_ga_0{i+1}"
                ),
                max_fitness=l,
            ),
            "multi_trial": multi_trial_one_plus_one_ga(l, mu, gens, trials, model),
        }
    if visualise:
        for j, (key, title) in enumerate(
            {
                "one_run": "Best Fitness per Generation in Different Versions of (1+1)-GA",
                "multi_trial": f"Average Best Fitness per Generation over {trials} Trials in Different Versions of (1+1)-GA",
            }.items()
        ):
            plot_fitnesses(
                {name: item[key] for name, item in results.items()},
                title=title,
                visuals_dir=visuals_dir,
                file_name=(
                    f"{file_name_prefix}fig_0{i+j+2}"
                    if file_name_prefix
                    else f"fig_0{i+j+2}"
                ),
                max_fitness=l,
                range_key=range_key,
            )


def get_frac(rate: float):
    if rate == 0:
        return "0"
    if rate % 1 == 0:
        return f"$\\frac{{{int(rate)}}}{{L}}$"
    f = fractions.Fraction(rate).limit_denominator(20)
    return f"$\\frac{{{f.numerator}}}{{{f.denominator}L}}$"


def string_search_multi_mu_multi_k(
    model: GeneticAlgorithm,
    target: str,
    ks: List[int],
    n: int,
    mu_rates: list[float],
    trials: int,
    max_gen: int,
    alphabet: List[str],
    p_c: float,
    k_distance: int,
    g_distance: int,
    visualise: bool = False,
    visuals_dir: str = "figures",
    file_name_prefix: Optional[str] = None,
    save_path: Optional[str] = None,
) -> Dict[float, List[int]]:
    """Perform multiple trials of multiple models (different mu and different k) of a string search model.

    Args:
        model (GeneticAlgorithm): A string search model.
        target (str): The target string.
        ks (List[int]): The list of ks to use.
        n (int): The population list.
        mu_rates (list[float]): The list of mu scales (x / l) to use.
        trials (int): The requested number of trials.
        max_gen (int): The maximum generations for which to run a trial.
        alphabet (List[str]): The alphabet.
        p_c (float): The crossover probability.
        k_distance (int): The number of samples of the population to take for calculating the mean Hamming distance.
        g_distance (int): The number of generations between recording the mean Hamming distance.
        visualise (bool, optional): _description_. Defaults to False.
        visuals_dir (str, optional): The path to the directory that stores the results. Defaults to "figures".
        file_name_prefix (Optional[str], optional): The prefix to the filename of each figure.
        If None, no prefix is used. Defaults to None.
        save_path (Optional[str], optional): The path to the saved results. If None, results are not loaded
        or saved. Defaults to None.

    Returns:
        Dict[float, Dict[int, Dict[str,List[Union[int, float, str]]]]]: The time to finish, the mean distances,
        and the last population for each mu and k.
    """
    get_model = lambda mu, k: model(
        target, mu, k, n, alphabet, p_c, max_gen, k_distance, g_distance
    )
    results = {}
    load = False
    if save_path:
        if os.path.isfile(save_path):
            load = True
            with open(save_path, "rb") as f:
                results = pickle.load(f)
    ln = len(target)
    if not load:
        for mu_rate in mu_rates:
            mu_val = mu_rate / ln
            mu_frac = get_frac(mu_rate)
            results[mu_frac] = {}
            for k in ks:
                t_finish = []
                distances = []
                populations = []
                m = get_model(mu_val, k)
                for _ in range(trials):
                    t, p, d = m.run()
                    t_finish.append(t)
                    distances.append(d)
                    populations.append(p)
                results[mu_frac][k] = {
                    "t_finish": t_finish,
                    "distances": distances,
                    "last_populations": populations,
                }
        if save_path:
            with open(save_path, "wb") as f:
                pickle.dump(results, f)
    if visualise:
        visualise_string_finders(
            results,
            ks,
            max_gen,
            visuals_dir=visuals_dir,
            file_name_prefix=file_name_prefix,
        )
    return results


def multi_model_tsp(
    data_paths: Dict[str, str],
    mu_scale: float,
    k: int,
    n: int,
    gens: int,
    trials: int,
    models: Dict[str, GeneticAlgorithm],
    visualise: bool = False,
    visuals_dir: str = "figures",
    file_name_prefix: Optional[str] = None,
    save_path: Optional[str] = None,
):
    """Solve TSP using multiple models and datasets.

    Args:
        data_paths (Dict[str, str]): The name of the dataset (key) and the path to the dataset (value).
        mu_scale (float): The scale to apply to mu such that mu = mu_scale / len(cities)
        k (int): the value of k to use.
        n (int): the size of the population.
        gens (int): The number of generations for which to run each trial.
        trials (int): The number of trials.
        models (Dict[str, GeneticAlgorithm]): The model name (key) and the model class (value).
        visualise (bool, optional): _description_. Defaults to False.
        visuals_dir (str, optional): The path to the directory that stores the results. Defaults to "figures".
        file_name_prefix (Optional[str], optional): The prefix to the filename of each figure.
        If None, no prefix is used. Defaults to None.
        save_path (Optional[str], optional): The path to the saved results. If None, results are not loaded
        or saved. Defaults to None.
    Returns:
        Dict[str, Dict[str, List[float]]]: The list of max fitnesses for each trial under each dataset and model.
    """
    data = {name: read_tsp(fpath) for name, fpath in data_paths.items()}
    results = {}
    load = False
    if save_path:
        if os.path.isfile(save_path):
            load = True
            with open(save_path, "br") as f:
                results = pickle.load(f)
    if not load:
        for data_name, dataset in data.items():
            results[data_name] = {}
            for model_name, model in models.items():
                m = model(dataset, mu_scale, k, n, gens)
                results[data_name][model_name] = [
                    max(m.run()[1]) for _ in range(trials)
                ]
        if save_path:
            with open(save_path, "wb") as f:
                pickle.dump(results, f)
    if visualise:
        multi_condition_beeswarm(
            results,
            title=f"Maximum Fitness Encountered over {trials} Trials of TSP on <name> Dataset per Algorithm",
            visuals_dir=visuals_dir,
            file_name_prefix=file_name_prefix,
        )
    return results
