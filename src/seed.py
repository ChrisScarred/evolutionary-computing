import random
import numpy as np

from config import SEED

random.seed(SEED)

def set_numpy_seed():
    np.random.seed(int.from_bytes(bytes(SEED, "utf-8"), byteorder='big') % (2**32-1))
