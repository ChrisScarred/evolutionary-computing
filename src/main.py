"""Load configs for each exercise and run their specific pipelines.
"""

from typing import Dict

from genetic_algorithm import (
    MemeticTravellingSalesman,
    OnePlusOne,
    OnePlusOneNoFitnessSelection,
    SimpleTravellingSalesman,
    StringFinder,
)
from pipe import (
    multi_model_one_plus_one,
    multi_model_tsp,
    selection_probs_multi_functions,
    string_search_multi_mu_multi_k,
)


def exercise_1(config: Dict, visuals_dir: str) -> None:
    """Run exercise 1 by using its specific config values in the multi-function selection probabilities pipeline."""
    selection_probs_multi_functions(
        config.get("fitness_functions", {}),
        config.get("population", []),
        visualise=True,
        visuals_dir=visuals_dir,
    )


def exercise_2(config: Dict, visuals_dir: str) -> None:
    """Run exercise 2 by using its specific config values in the multi-model (1+1)-GA pipeline."""
    l = config.get("l")
    multi_model_one_plus_one(
        l,
        config.get("mu_scale") / l,
        config.get("gens"),
        config.get("trials"),
        {
            "(1+1)-GA with Fitness-based Selection": {
                "model": OnePlusOne,
                "visualise": True,
            },
            "(1+1)-GA with Non-selective Generational Replacement": {
                "model": OnePlusOneNoFitnessSelection,
                "visualise": False,
            },
        },
        visualise=True,
        visuals_dir=visuals_dir,
        file_name_prefix="ex_02_",
        range_key="(1+1)-GA with Non-selective Generational Replacement",
    )


def exercise_3(config: Dict, visuals_dir: str) -> None:
    """Run exercise 3 by using its specific config values in the multi-mu multi-k StringFinder pipeline."""
    string_search_multi_mu_multi_k(
        StringFinder,
        config.get("target"),
        config.get("ks"),
        config.get("n"),
        config.get("mu_scale"),
        config.get("trials"),
        config.get("max_gen"),
        config.get("alphabet"),
        config.get("p_c"),
        config.get("distance_gens"),
        config.get("distance_k"),
        visualise=True,
        visuals_dir=visuals_dir,
        file_name_prefix="ex_03_",
        save_path=config.get("save_path"),
    )


def exercise_4(config: Dict, visuals_dir: str) -> None:
    """Run exercise 4 by using its specific config values in the multi-model TSP pipeline."""
    multi_model_tsp(
        config.get("datasets"),
        config.get("mu_scale"),
        config.get("k"),
        config.get("n"),
        config.get("gens"),
        config.get("trials"),
        {
            "Simple TSP GA": SimpleTravellingSalesman,
            "Memetic TSP GA with 2-opt": MemeticTravellingSalesman,
        },
        visualise=True,
        visuals_dir=visuals_dir,
        file_name_prefix="ex_04_",
        save_path=config.get("save_path"),
    )


def dispatch(config: Dict):
    """Run all requested exercises given the config values."""
    actions = config.get("run_exercises", [])
    visuals_dir = config.get("visuals_dir")
    for i in actions:
        exercise_specific_config = config.get(f"ex_0{i}")
        match i:
            case 1:
                exercise_1(exercise_specific_config, visuals_dir)
            case 2:
                exercise_2(exercise_specific_config, visuals_dir)
            case 3:
                exercise_3(exercise_specific_config, visuals_dir)
            case 4:
                exercise_4(exercise_specific_config, visuals_dir)
            case _:
                print(f"Unknown action: {i}")


if __name__ == "__main__":
    from config import CONFIG

    dispatch(CONFIG)
