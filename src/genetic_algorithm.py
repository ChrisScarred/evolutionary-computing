"""Implementations of different genetic algorithms and their abstract (base) class.
"""

import math
from seed import *
from abc import ABC, abstractmethod
from itertools import product
from typing import List, Set, Tuple

import numpy as np

City = Tuple[float, float]
Path = Set[City]
Population = List[Path]


class GeneticAlgorithm(ABC):
    """Base class for genetic algorithms (GA) containing the necessary components of a GA:
    - initiation of the population,
    - creation of the new generation,
    - a selection procedure,
    - a run function which contains the necessary procedures for iterative runs of the GA.
    """

    @abstractmethod
    def new_generation(self):
        """Abstract method for creation of a new generation.

        Implementations may utilise a tournament selection,
        crossover, mutation, or local search (not an exhaustive list).
        """
        pass

    @abstractmethod
    def selection(self):
        """Abstract method for selection of the next generation.

        Implementations may utilise a generational replacement,
        (partial) elitism, or other components, often based on a fitness function.
        """
        pass

    @abstractmethod
    def init_population(self):
        """Abstract method for creation of the initial population.

        Implementations may utilise a random generator or a fixed initial population.
        """
        pass


class OnePlusOne(GeneticAlgorithm):
    """An elitistic GA with mutation for solving the (1+1) problem.

    Randomly initialises the population to an l-length binary string.
    New generation is created by mutation of the parent.
    Elitistic selection is employed to select between the parent and the child.
    Fitness function is the sum of the string.

    Attributes:
        mu (float): the mutation probability of each bit.
        l (int): the length of the string whose value to maximise.
        gens (int): the number of generations for which to run the algorithm.
    """

    def __init__(self, mu: float, l: int, gens: int) -> None:
        self.mu = mu
        self.l = l
        self.generations = gens

    def run(self) -> Tuple[List[int], List[int]]:
        """Perform a single trial of the (1+1) GA.

        Returns:
            Tuple[List[int], List[int]]: the best individual and the best fitness per generation.
        """
        x = self.init_population()
        fitnesses = [self.fitness(x)]
        for _ in range(self.generations):
            xm = self.new_generation(x)
            x, fitness = self.selection(x, xm)
            fitnesses.append(fitness)
        return x, fitnesses

    def init_population(self) -> List[int]:
        """Initialise the population by creating a random list of binary integers."""
        return [random.randrange(2) for _ in range(self.l)]

    def new_generation(self, parent: List[int]) -> List[int]:
        """Obtain a new generation by mutating the parent."""
        mutate = lambda x: 1 - x if random.random() < self.mu else x
        return [mutate(n) for n in parent]

    def fitness(self, subject: List[int]) -> int:
        """Calculate the fitness as the sum of the list."""
        return np.sum(np.array(subject))

    def selection(self, parent: List[int], child: List[int]) -> Tuple[List[int], int]:
        """Perform elitist selection and return the selected individual
        along with its fitness.
        """
        parent_fitness = self.fitness(parent)
        child_fitness = self.fitness(child)
        return (
            (parent, parent_fitness)
            if parent_fitness > child_fitness
            else (child, child_fitness)
        )


class OnePlusOneNoFitnessSelection(OnePlusOne):
    """A GA with mutation and generational replacement for solving the (1+1) problem.

    Randomly initialises the population to an l-length binary string.
    New generation is created by mutation of the parent and always selecting the child regardless of fitness.

    Attributes:
        mu (float): the mutation probability of each bit.
        l (int): the length of the string whose value to maximise.
        gens (int): the number of generations for which to run the algorithm.
    """

    def selection(self, parent: List[int], child: List[int]) -> Tuple[List[int], int]:
        """Perform selection by generational replacement and return the selected individual along with its fitness."""
        return child, self.fitness(child)


class StringFinder(GeneticAlgorithm):
    """A GA with tournament selection of parents, child crossover, child mutation, and generational replacement for finding an unknown target string.

    Attributes:
        target (str): the target string.
        mu (float): the mutation probability.
        k (int): the size of the sample of the population that enters the tournament selection.
        population_size (int): the size of the population.
        alphabet (List[str]): the search space as a list of characters.
        p_c (float): The crossover probability.
        g_max (int): The maximum number of generations for which the search runs.
        distance_sample (int): The size of the sample randomly selected to calculate the mean distance among the population.
        distance_gens (int): The number of generations between recording the mean distance among the population.
    """

    def __init__(
        self,
        target: str,
        mu: float,
        k: int,
        population_size: int,
        alphabet: List[str],
        p_c: float,
        g_max: int,
        distance_sample: int,
        distance_gens: int,
    ) -> None:
        self.target = target
        self.mu = mu
        self.k = k
        self.population_size = population_size
        self.alphabet = alphabet
        self.p_c = p_c
        self.g_max = g_max
        self.k_distance = distance_sample
        self.g_distance = distance_gens

    def tournament(self, population) -> str:
        """Perform tournament selection with sample size k."""
        return max(random.sample(population, self.k), key=self.fitness)

    def fitness(self, subject: str) -> int:
        """Calculate the fitness of a subject by returning the number of correct characters
        (whose position matches)."""
        return sum(
            [
                1 if subject[i] == self.target[i] else 0
                for i in range(len(self.target) - 1)
            ]
        )

    def selection(self, proposed_population: List[str]) -> List[str]:
        """Perform selection by generational replacement."""
        return proposed_population

    def random_string(self) -> str:
        """Obtain a random string consisting only of characters in this instance's alphabet."""
        return "".join(random.choices(self.alphabet, k=len(self.target)))

    def init_population(self) -> List[str]:
        """Initialise the population by generating a random string for each individual."""
        return [self.random_string() for _ in range(self.population_size)]

    def crossover(self, parents: List[str]) -> List[str]:
        """Perform crossover by randomly selecting a crossover point and using the first part of one parent
        and the second part of the other to obtain two children.
        """
        if np.random.random() < self.p_c:
            c = random.randrange(1, len(self.target) - 1)
            return [parents[0][:c] + parents[1][c:], parents[1][:c] + parents[0][c:]]
        return parents

    def mutation(self, subject: str) -> str:
        """Perform a mutation of the subject by randomly replacing each character with
        the probability mu.
        """
        return "".join(
            [
                c if random.random() >= self.mu else random.choice(self.alphabet)
                for c in subject
            ]
        )

    def new_generation(self, old_gen: List[str]) -> List[str]:
        """Obtain a new generation from the old generation by selecting two parents in a tournament,
        performing crossover and mutation on the children until the size of the new generation
        reaches the size of the old generation.
        """
        new_gen = []
        i = 0
        while i < self.population_size:
            parents = [self.tournament(old_gen) for _ in range(2)]
            children = [self.mutation(x) for x in self.crossover(parents)]
            i += len(children)
            new_gen.extend(children)
        return new_gen

    def mean_hamming_distance(self, population: List[str]) -> float:
        """Calculate mean Hamming distance of a sample of the population."""
        sample = random.sample(population, k=self.k_distance)
        hamming_distance = lambda x, y: sum(
            1 if x[i] != y[i] else 0 for i in range(len(self.target))
        )
        return np.mean(
            np.array(
                [
                    hamming_distance(x, y)
                    for ((i, x), (j, y)) in product(
                        enumerate(sample), enumerate(sample)
                    )
                    if i != j
                ]
            )
        )

    def run(self) -> Tuple[int, List[str], List[float]]:
        """Perform a single run of string searching.

        Returns:
            Tuple[int, List[str], List[float]]: Time to finish, the last generation, and the mean Hamming distances
            for each measuring point.
        """
        distances = []
        population = self.init_population()
        for i in range(self.g_max):
            if i % self.g_distance == 0:
                distances.append(self.mean_hamming_distance(population))
            best_individual = max(population, key=self.fitness)
            if best_individual == self.target:
                break
            new_gen = self.new_generation(population)
            population = self.selection(new_gen)
        return i + 1, population, distances


class SimpleTravellingSalesman(GeneticAlgorithm):
    """A GA with tournament selection of parent, child crossover, child mutation, and generational replacement to solve the TSP.

    Attributes:
        cities (Path): The list of cities as their coordinates.
        mu (float): The mutation probability.
        k (int): The size of the sample of the population that enters the tournament selection.
        n (int): The population size.
        gens (int): The number of generations for which to run the algorithm.
    """

    def __init__(
        self, cities: Path, mu_scale: float, k: int, n: int, gens: int
    ) -> None:
        self.cities = cities
        self.mu = mu_scale / len(cities)
        self.k = k
        self.n = n
        self.gens = gens

    def euclidean_distance(self, a: City, b: City) -> float:
        """Euclidean distance between a city a and a city b."""
        return math.sqrt(sum([(ai - bi) ** 2 for ai, bi in zip(a, b)]))

    def fitness(self, path: Path) -> float:
        """The fitness of an individual as the reciprocal of the total Euclidean distance
        between each adjacent city pair in the path.
        """
        return 1 / sum(
            self.euclidean_distance(a, b) for a, b in zip(path, path[1:] + [path[0]])
        )

    def crossover(self, parents: Population) -> Population:
        """Perform the crossover between parents by selecting two random indices on their paths,
        extracting the part of the path between the two indices and combining it with the right-side complement
        of the other parent to the left side of the child and left-side complemenet of the other parent
        to the right side of the child."""
        n = len(self.cities)
        c1 = random.randrange(1, n - 2)
        c2 = random.randrange(c1, n)
        components = [(x[:c1], x[c1:c2], x[c2:]) for x in parents]
        children = []
        p = len(parents)
        for i in range(p):
            mid = components[i][1]
            complement = [j for j in parents[(i + 1) % p] if j not in mid]
            child = complement[(c2 - len(mid)) :] + mid + complement[:c1]
            children.append(child)
        return children

    def mutation(self, path: Path) -> Path:
        """Perform mutation of an individual by randomsly selecting two cities and swapping them with probability mu
        repeated l/2 times.
        """
        for _ in range(round(len(self.cities) / 2)):
            if random.random() < self.mu:
                c1 = random.randrange(0, len(self.cities))
                c2 = random.choice([i for i in range(len(self.cities)) if i != c1])
                v1 = path[c1]
                path[c1] = path[c2]
                path[c2] = v1
        return path

    def selection(self, population: Population) -> Population:
        """Perform selection as generational replacement."""
        return population

    def tournament(self, population: Population) -> Path:
        """Perform tournament selection on the population with sample size k."""
        return max(random.sample(population, self.k), key=self.fitness)

    def new_generation(self, old_gen: Population) -> Population:
        """Create new generation from the old generation by selecting two parents via a tournament, performing parent crossover
        and mutation untill the size of the new generation matches the size of the old one.
        """
        children = []
        for _ in range(round(self.n / 2)):
            parents = [self.tournament(old_gen) for _ in range(2)]
            children.extend([self.mutation(x) for x in self.crossover(parents)])
        return children

    def init_population(self) -> Population:
        """Initialise the population by randomly ordering the list of cities for each individual."""
        return [random.sample(self.cities, k=len(self.cities)) for _ in range(self.n)]

    def run(self) -> Tuple[Path, List[float]]:
        """Perform a single trial of TSP.

        Returns:
            Tuple[Path, List[float]]: The best path and the list of best fitness per generation.
        """
        population = self.init_population()
        fitnesses = [max([self.fitness(p) for p in population])]
        for _ in range(self.gens):
            new_generation = self.new_generation(population)
            population = self.selection(new_generation)
            fitnesses.append(max([self.fitness(p) for p in population]))
        return max(population, key=self.fitness), fitnesses


class MemeticTravellingSalesman(SimpleTravellingSalesman):
    """A memetic GA with tournament selection of parent, child crossover, and generational replacement to solve the TSP.

    The memetic component is a 2-opt local search that replaces child mutation by swapping pairs of cities in a path until a fitter option is found.
    Note that 2-opt typically swaps all pairs of cities to create a population from which the best performing individual is selected, but we chose
    an early-stopping version for performance concerns. Furthermore, some implementations of 2-opt select all pairs of cities and instead
    of swapping them, reverse the path between them. We chose the swapping for better performance.
    Fitness is measured as the reciprocal of the total Euclidean distance of the path.

    Attributes:
        cities (Path): The list of cities as their coordinates.
        mu (float): The mutation probability.
        k (int): The size of the sample of the population that enters the tournament selection.
        n (int): The population size.
        gens (int): The number of generations for which to run the algorithm.
    """

    def param_combinations(self) -> List[Tuple[int, int]]:
        """Get all index combinations of the paths where the first index is smaller than the second:
        aka all swappable pairs of cities as indices.
        """
        n = len(self.cities)
        return [(i, j) for (i, j) in product(range(n), range(n)) if i < j]

    def switch(self, subject: Path, v1: int, v2: int) -> Path:
        """Switch the city at index `v1` with the city at index `v2` in a path `subject`."""
        val = subject[v1]
        subject[v1] = subject[v2]
        subject[v2] = val
        return subject

    def _2_opt(self, path: Path) -> Path:
        """Given a path, run 2-opt local search with city-swapping strategy until a fitter path is encountered, then return the fitter path.

        If no fitter path is found, the original path is returned.
        """
        fitness = self.fitness(path)
        for v1, v2 in self.param_combinations():
            alt = self.switch(path, v1, v2)
            if self.fitness(alt) > fitness:
                return alt
        return path

    def new_generation(self, old_gen: Population) -> Population:
        """Given an old generation, generate a new generation by iteratively selecting two parents in a tournament,
        performing a crossover to obtain two children, and performing 2-opt on each child until the size
        of the new population is the same as the size of the old population.
        """
        children = []
        for _ in range(round(self.n / 2)):
            parents = [self.tournament(old_gen) for _ in range(2)]
            children.extend([self._2_opt(x) for x in self.crossover(parents)])
        return children
