"""Configuration dictionary.
"""

import string

SEED = "GottaMakeThisReproducible"
CONFIG = {
    # the list of exercises whose code you wish to run
    "run_exercises": [1, 2, 3, 4],
    # the directory where figures are saved
    "visuals_dir": "figures",
    # configuration for the 1st exercise
    "ex_01": {
        "fitness_functions": {
            "f1": lambda x: abs(x),
            "f2": lambda x: x**2,
            "f3": lambda x: 2 * x**2,
            "f4": lambda x: x**2 + 20,
        },
        "population": [2, 3, 4],
    },
    # configuration for the 2nd exercise
    "ex_02": {
        "l": 100,
        # mu_scale specifies the scaling of mutation value mu in the following way: mu = mu_scale / l
        "mu_scale": 1,
        "gens": 1500,
        "trials": 10,
    },
    # configuration for the 3rd exercise
    "ex_03": {
        # the target string to search for
        "target": "GeneticAlgorithm",
        # sample size used for tournament selection of a parent
        # provided as a list to run trials with different values
        "ks": [2, 5],
        # population size
        "n": 200,
        # mu_scale specifies the scaling of mutation value mu in the following way: mu = mu_scale / l
        # l is the length of the target string
        # this exercise utilises multiple mu scales to compare the effect of different values of mu
        "mu_scale": [0.2 * x for x in range(10)] + [3],
        # the number of trials for which the StringFinder runs
        "trials": 10,
        # early stopping criterion as the maximum allowed generation in the case the target string is not found sooner
        "max_gen": 1000,
        # the alphabet to use in the search
        "alphabet": string.ascii_lowercase + string.ascii_uppercase,
        # crossover probability
        "p_c": 1,
        # record the mean Hamming distance every distance_gens
        "distance_gens": 10,
        # the size of the sample used to compute mean Hamming distance
        "distance_k": 20,
        # if you wish to save the results of the run to speed up the subsequent runs,
        # provide a path for a pickle file that will store the results
        # else set this setting to None
        "save_path": "data/ex_03.pickle",
    },
    # configuration for the 4th exercise
    "ex_04": {
        # paths to the datasets used
        "datasets": {
            # the default dataset provided with the assignment
            "file-tsp": "data/file-tsp.txt",
            # a custom dataset from TSPLIB, may be replaced with any other dataset in (zipped) .tsp format
            "pr76": "data/pr76.tsp.gz",
        },
        # mu_scale specifies the scaling of mutation value mu in the following way: mu = mu_scale / l
        # it this exercise, l is the total number of locations
        "mu_scale": 1,
        # sample size used for tournament selection of a parent
        "k": 2,
        # population size
        "n": 10,
        # the number of generations for which to run each algorithm
        "gens": 1500,
        # the number of trials for which to run each algorithm
        "trials": 10,
        # if you wish to save the results of the run to speed up the subsequent runs,
        # provide a path for a pickle file that will store the results
        # else set this setting to None
        "save_path": "data/ex_04.pickle",
    },
}
