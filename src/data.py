"""Input data reading.
"""
import gzip
from typing import List, Optional, Tuple


def read_lines(file_path: str) -> List[str]:
    """Read lines of a potentially zipped file.
    """
    if file_path.endswith(".gz"):
        with gzip.open(file_path, "rt") as f:
            return f.readlines()
    with open(file_path, "r") as f:
        return f.readlines()


def parse_tsp_line(line: str) -> Optional[Tuple[float, float]]:
    """Parse a TSP line.
    """
    components = line.strip().split()
    if len(components) == 3:
        components = components[1:]
    if len(components) == 2:
        try:
            return tuple(map(float, components))
        except ValueError:
            pass


def read_tsp(file_path) -> List[Tuple[float, float]]:
    """Read a potentially zipped TSP file.
    """
    coords = [parse_tsp_line(x) for x in read_lines(file_path)]
    return [c for c in coords if c]
