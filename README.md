# Natural Computing Assignment 5: Evolutionary Computing

## Instructions

1. Obtain dependencies specified in `pyproject.toml`. Note that:
   1. Only the versions specified there were tested, but more versions likely work.
   2. The `match-case` statement in `dispatch(config)` in `src/main.py` requires Python 3.10 or higher. It may be rewritten to a chain of `if-else` statements if you wish to use a lower Python version.
   3. The package `pyside2` is required only to show figures during the runtime and hence may be omitted if you wish or if it does not work with Windows (it was only tested on Linux).
2. Edit the configuration file in `src/config.py` if necessary.
   1. To reproduce our results for exercise 3, use the target string `GeneticAlgorithm` and 500 maximum generations.
   2. To reproduce our results for exercise 4, use `file-tsp.txt` provided with the assignment and `pr76.tsp.gz` from [Ruprecht-Karls-Universität Heidelberg's Symmetric traveling salesman problem collection of the TSPLIB Library](http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/tsp/).
3. Run `python src/main.py` from the root directory of this project.
